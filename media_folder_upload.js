(function ($) {
  Drupal.behaviors.media_folder_upload = {
    attach: function (context) {
      $('div.folder_load').bind('click', function( event ) {
        // grab item
        var $item = $(this);
        // and change folder field value
        $('#edit-field-folder').val($item.attr('id').replace('folder_load_', ''));
        return false;
      });
    }
  };
})(jQuery);
